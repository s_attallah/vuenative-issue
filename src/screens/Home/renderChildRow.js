import React, {Component} from 'react';
import { Platform, View } from 'react-native';
import { ListItem, Icon, Right, Left, Body, Text } from 'native-base';
import commonColor from '../../../native-base-theme/variables/commonColor';

export default class RenderChildRow extends Component {
    render() {        
        return (
            <ListItem icon style={{borderBottomWidth: 1, marginTop: 5, marginBottom: 5}}>
                <Left>
                    <Icon active name={this.props.data.icon} style={{ width: 30 }} />
                </Left>
                <Body>
                    <Text style={{color: commonColor.inverseTextColor}}>
                        {this.props.data.listData}
                    </Text>
                </Body>
                <Right>
                    <Text style={{ fontWeight: "400", paddingTop: 18}} note>
                        {this.props.data.time}
                    </Text>
                </Right>
            </ListItem>
        );
    }
}
