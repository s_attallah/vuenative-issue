This project was bootstrapped with [Create React Native App](https://github.com/react-community/create-react-native-app).

Below you'll find information about performing common tasks. The most recent version of this guide is available [here](https://github.com/react-community/create-react-native-app/blob/master/react-native-scripts/template/README.md).

##Issue Example

Please clone the repo and run:

```
npm install

//then run on device or similator of your chance (i use IOS)
npm run ios
```

**Goal:**
Display dynamically images sources.
Get images src from a json file `src/assets/data/data.json`

###Code

You'll find the code into `src/assets/screens/Hone/index.vue`
I get my images src from my json (l.31 - see the console.log with expected datas)
then i loop into my array of sources ('singers' - l.25)
and try to display images dynamcally with sources (l.6)

```
<image :style="{width: 150, height: 150}" :source="require(`../../assets/ffs/${src}`)"/>
```

Display following error:

###Error

```
/Users/technical/Documents/bitbucket/test/vuenative-nativestarterpro-clone/vue-native/src/screens/Home/index.vue: /Users/technical/Documents/bitbucket/test/vuenative-nativestarterpro-clone/vue-native/src/screens/Home/index.vue:54:20: calls to `require` expect exactly 1 string literal argument, but this was found: `require("../../assets/ffs/" + src)`.

ABI27_0_0RCTFatal
__37-[ABI27_0_0RCTCxxBridge handleError:]_block_invoke
_dispatch_call_block_and_release
_dispatch_client_callout
_dispatch_main_queue_callback_4CF
__CFRUNLOOP_IS_SERVICING_THE_MAIN_DISPATCH_QUEUE__
__CFRunLoopRun
CFRunLoopRunSpecific
GSEventRunModal
UIApplicationMain
main
start
0x0
```